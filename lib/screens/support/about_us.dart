import 'package:aldar_broker/screens/base/base_stateless_screen.dart';

import '../../widgets/aldar_base_appbar.dart';
import 'package:flutter/material.dart';
import '../../app/app_localizations.dart';
import '../../app/app_theme.dart';
import '../../widgets/main_drawer.dart';

class AboutUS extends BaseStatelessScreen {
  static const routeName = '/about_us';

  @override
  String screenName() =>'about_us';

  @override
  bool showDrawer() => true;

  @override
  Widget buildBody(BuildContext context) {
    // TODO: implement buildBody
    return Center(
      child: Text(AppTranslations.of(context).text("about_us")),
    );
  }


}