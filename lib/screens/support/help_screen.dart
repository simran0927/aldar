

import 'package:aldar_broker/app/app_localizations.dart';
import 'package:aldar_broker/screens/base/base_state.dart';
import 'package:aldar_broker/screens/base/base_stateful_widget.dart';
import 'package:aldar_broker/screens/base/base_statefull_screen.dart';
import 'package:aldar_broker/widgets/aldar_base_appbar.dart';
import 'package:aldar_broker/widgets/main_drawer.dart';
import 'package:aldar_broker/widgets/rounded_button.dart';
import 'package:flutter/material.dart';

class HelpScreen extends BaseStatefulWidget {
  static const routeName = '/help';

  HelpScreen({Key key}) : super(key: key);

  @override
  _HelpScreenState createState() => _HelpScreenState();
}

class _HelpScreenState extends BaseState<HelpScreen> with BaseStatefulScreen{

  @override
  String screenName() =>'help';

  @override
  bool showDrawer() => true;

  @override
  Widget buildBody(BuildContext context) {
    // TODO: implement buildBody
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top,
              left: 16,
              right: 16),
          child: Image.asset('assets/images/helpImage.png'),
        ),
        Container(
          padding: const EdgeInsets.only(top: 8),
          child: Text(
            AppTranslations.of(context).text('help_headline'),
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),

        Container(
          padding: const EdgeInsets.only(top: 16),
          child: Text(
            AppTranslations.of(context).text('help_description'),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: Container(
                width: 120,
                height: 40,
                child: Material(
                  color: Colors.transparent,
                  child: RoundedCornerButton(
                          () {
                        showMessage('Hellow');
                      },
                      AppTranslations.of(context).text('chat_with_us')),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
