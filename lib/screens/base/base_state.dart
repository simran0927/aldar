import 'dart:io';

import 'package:aldar_broker/app/app_theme.dart';
import 'package:aldar_broker/widgets/aldar_base_appbar.dart';
import 'package:aldar_broker/widgets/main_drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'base_screen.dart';
import 'base_stateful_widget.dart';

abstract class BaseState<Page extends BaseStatefulWidget> extends State<Page> with BaseScreen{
  bool _isLoading = false;
  BuildContext context;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    return Scaffold(
        appBar: AldarBaseAppbar(title: screenName(),
          appBar: AppBar(),
          widgets: setAppbarWidgets(),),
        drawer: showDrawer() ? HomeDrawer() : null,
        body: Container(
            color: AppTheme.pageBackground,
            padding: AppTheme.main_block_padding,
            child: _isLoading
                ? Center(child: CircularProgressIndicator(),)
                :buildBody(context)
        )
    );
  }

  @override
  bool showDrawer() => false;
  @override
  List<Widget> setAppbarWidgets() =>null;

  /// show progress bar
  void showProgress() {
    setState(() {
      _isLoading = true;
    });
  }

  /// hide progress bar
  void hideProgress() {
    setState(() {
      _isLoading = false;
    });
  }
  @override
  void onHideProgress() {
    hideProgress();
  }

  @override
  void onShowProgress() {
    showProgress();
  }

  @override
  void onError(Object e, {StackTrace stackTrace}) {
    hideProgress();
    if (stackTrace != null) print(stackTrace);
    if (e is SocketException) {
      showMessage('No Network Avaiable');
    }else {
      showMessage('Unexpected Error Occured');
    }
  }

  /// show message: toast on android or alert dialog on ios
  void showMessage(String message) {
    if (message == null) {
      return;
    }
    if (Platform.isIOS) {
      showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          content: Text(message),
          //title: Text(''),
          actions: [
            CupertinoDialogAction(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );
    } else {
      Fluttertoast.showToast(
          msg: message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          textColor: Colors.white,
          fontSize: 14.0);
    }
  }

}