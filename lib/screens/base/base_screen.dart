
import 'package:flutter/material.dart';

mixin BaseScreen{
  /// should be overridden in extended widget
  String screenName();
  /// should be overridden in extended widget
  bool showDrawer();

  List<Widget> setAppbarWidgets();

  /// should be overridden in extended widget
  Widget buildBody(BuildContext context);

  void onError(Object e);
  void onShowProgress();
  void onHideProgress();
}