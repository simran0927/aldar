import 'package:aldar_broker/app/app_theme.dart';
import 'package:aldar_broker/widgets/aldar_base_appbar.dart';
import 'package:aldar_broker/widgets/main_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'base_screen.dart';
import 'base_stateless_widget.dart';

abstract class BaseStatelessScreen extends BaseStatelessWidget with BaseScreen{

  @override
  Widget getLayout(BuildContext context) {
    return Scaffold(
        appBar: AldarBaseAppbar(title: screenName(),
          appBar: AppBar(),
          widgets: setAppbarWidgets(),),
        drawer: showDrawer() ? HomeDrawer() : null,
        body: Container(
            color: AppTheme.pageBackground,
            padding: AppTheme.main_block_padding,
            child:buildBody(context)
        )
    );
  }

  @override
  void onHideProgress() {}
  @override
  void onShowProgress() {}
  @override
  List<Widget> setAppbarWidgets()=>null;
  @override
  bool showDrawer() => false;

  @override
  void onError(Object e) {
    // TODO: implement onError
  }

}