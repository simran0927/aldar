import '../../widgets/aldar_base_appbar.dart';
import 'package:flutter/material.dart';
import '../../app/app_localizations.dart';
import '../../app/app_theme.dart';
import '../../widgets/main_drawer.dart';
import '../../widgets/button_rectangle.dart';
import '../../app/application.dart';


class SettingsScreen extends StatefulWidget {
  static const routeName = '/settings';

  static final List<String> languagesList = application.supportedLanguages;
  static final List<String> languageCodesList =
      application.supportedLanguagesCodes;

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
    final Map<dynamic, dynamic> languagesMap = {
    SettingsScreen.languagesList[0]: SettingsScreen.languageCodesList[0],
    SettingsScreen.languagesList[1]: SettingsScreen.languageCodesList[1],
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AldarBaseAppbar(title:'settings',
            appBar: AppBar()),
    drawer: HomeDrawer(),
    body: Center(
      child: _buildLanguagesList(),
    ) ,
    );
  }

  _buildLanguagesList() {
    return ListView.builder(
      itemCount: SettingsScreen.languagesList.length,
      itemBuilder: (context, index) {
        return _buildLanguageItem(SettingsScreen.languagesList[index]);
      },
    );
  }

  _buildLanguageItem(String language) {
    print(AppTranslations.of(context).locale.languageCode);

    return InkWell(
      onTap: () {
        print(language);
        application.onLocaleChanged(Locale(languagesMap[language]));
      },
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 20.0),
          child: Text(
            language,
            style: TextStyle(
              fontSize: 24.0,
              color: AppTranslations.of(context).locale.languageCode == languagesMap[language]? Colors.red :Colors.black
            ),
          ),
        ),
      ),
    );
  }
}