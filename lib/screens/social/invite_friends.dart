import 'dart:wasm';

import '../../widgets/aldar_base_appbar.dart';
import 'package:flutter/material.dart';
import '../../app/app_localizations.dart';
import '../../app/app_theme.dart';
import '../../widgets/main_drawer.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class InviteFriends extends StatefulWidget {
  static const routeName = '/invite';

  @override
  _InviteFriendsState createState() => _InviteFriendsState();
}

class _InviteFriendsState extends State<InviteFriends> {

List<charts.Series<Task,String>> _seriesPieData;

_generateData(){
  var pieData = [
    new Task("Work", 35.8, Color(0xff3366cc)),
     new Task("Eat", 8.3, Color(0xff990099)),
      new Task("Commute", 10.8, Color(0xff109618)),
       new Task("Tv", 15.6, Color(0xfffdbe19)),
        new Task("Sleep", 19.2, Color(0xffff9900)),
         new Task("Other",10.3, Color(0xffdc3912)),
  ];

  _seriesPieData.add(
    charts.Series(
      data:pieData,
      domainFn: (Task task,_) => task.task,
      measureFn: (Task task, _) => task.taskValue,
      colorFn: (Task task,_)=> charts.ColorUtil.fromDartColor(task.colorVal),
      id:'Daily Task',
      labelAccessorFn: (Task row, _)=>'${row.taskValue}'
    ),
  );
}

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    _seriesPieData = List<charts.Series<Task,String>>();
    _generateData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AldarBaseAppbar(title:'invite_friend',
            appBar: AppBar()),
    drawer: HomeDrawer(),
    body: SingleChildScrollView(
          child: Center(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Container(
               padding: EdgeInsets.only(left: 10),
              width:  double.infinity,
              height: 400,
              child: charts.BarChart(
                _seriesPieData,
                animate: true,
                animationDuration: Duration(milliseconds: 500),
                behaviors: [
                  new charts.DatumLegend(
                    outsideJustification: charts.OutsideJustification.endDrawArea,
                    horizontalFirst: false,
                    desiredMaxRows: 2,
                    cellPadding: new EdgeInsets.only(right: 4.0, bottom: 4.0),
                    entryTextStyle: charts.TextStyleSpec(
                      color: charts.MaterialPalette.purple.shadeDefault,
                      fontFamily: 'Georgia',
                      fontSize: 11
                      )
                  )
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width:  double.infinity,
              height: 400,
              child: charts.PieChart(
                _seriesPieData,
                animate: true,
                animationDuration: Duration(milliseconds: 500),
                defaultRenderer: new charts.ArcRendererConfig(
                  arcWidth: 60,
                  arcRendererDecorators: [
                    new charts.ArcLabelDecorator(
                    labelPosition: charts.ArcLabelPosition.inside
                    )
                  ]
                ),
                behaviors: [
                  new charts.DatumLegend(
                    outsideJustification: charts.OutsideJustification.endDrawArea,
                    horizontalFirst: false,
                    desiredMaxRows: 2,
                    cellPadding: new EdgeInsets.only(right: 4.0, bottom: 4.0),
                    entryTextStyle: charts.TextStyleSpec(
                      color: charts.MaterialPalette.purple.shadeDefault,
                      fontFamily: 'Georgia',
                      fontSize: 11
                      )
                  )
                ],
              ),
            ),
          ],
        )
      ),
    ) ,
    );
  }
}


class Task {
  String task;
  double taskValue;
  Color colorVal;

  Task(this.task,this.taskValue,this.colorVal);
}