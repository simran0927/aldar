import 'package:aldar_broker/screens/base/base_stateless_screen.dart';

import '../../widgets/aldar_base_appbar.dart';
import 'package:flutter/material.dart';
import '../../app/app_localizations.dart';
import '../../widgets/main_drawer.dart';

class RateApp extends BaseStatelessScreen {
  static const routeName = '/rate_app';


  @override
  Widget buildBody(BuildContext context) {
    // TODO: implement buildBody
    return Center(
      child: Text(AppTranslations.of(context).text("rate_app")),
    );
  }

  @override
  String screenName() =>'rate_app';

  @override
  bool showDrawer() => true;


}