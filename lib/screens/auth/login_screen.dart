import 'dart:async';
import '../../widgets/email_Field.dart';
import '../../widgets/input_field.dart';
import 'package:flutter/material.dart';
import '../../app/app_theme.dart';
import '../dashboard/dashboard.dart';
import '../../widgets/rounded_button.dart';
import '../../widgets/email_Field.dart';
import '../../widgets/password_field.dart';
import '../../widgets/my_flat_button.dart';
import '../../app/app_localizations.dart';
import '../../widgets/rounded_button.dart';
import '../../provider/auth/auth_provider.dart';
import 'package:provider/provider.dart';


class LoginScreen extends StatefulWidget {
  static const routeName = '/login';
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {

  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  
  void navigateToHomePageoPage() {
     final authProvider = Provider.of<AuthProvider>(context, listen: false);
     authProvider.updateLoggedInsStatus(true);
    Navigator.of(context).pushReplacementNamed(DashboardScreen.routeName);
  }
  @override
  void initState() {
    super.initState();
  }
  void forgotPasswordAction() {

  }
  void signUpAction(){

  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: AppTheme.mainAppThemeColor,
    body: new Center(
      child: SingleChildScrollView(
          child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                width: 200,
                height: 200,
                child: new Image.asset('assets/images/app_logo.png',
                ),
              ),
              SizedBox(height: 50,),
              RoundedCornerInput(hintText: 'Email', isSecureInput: false,),
              SizedBox(height: 10,),
              RoundedCornerInput(hintText: 'Password', isSecureInput: true,),
              //PasswordFiled(this.passwordController),
              SizedBox(height: 10,),
              Container(
                width: double.infinity,
                height: 50,
                child: RoundedCornerButton(navigateToHomePageoPage, AppTranslations.of(context).text("login"))),
                SizedBox(height:10),
                MyFlatButton(this.signUpAction,AppTranslations.of(context).text("sign_up")),
                MyFlatButton(this.forgotPasswordAction,AppTranslations.of(context).text("forgot_password")),
            ],
          ),
        ),
      ),
    ),
  );
  }
}
