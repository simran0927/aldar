import 'dart:async';
import 'package:flutter/material.dart';
import '../../app/app_theme.dart';
import '../properties/properties.dart';
import '../auth/login_screen.dart';
import '../../provider/auth/auth_provider.dart';
import 'package:provider/provider.dart';



class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  bool isLoggedIn = false;
  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    print('Timer involked');
        final authProvider = Provider.of<AuthProvider>(context, listen: false);

    if(authProvider.getLoggedInStatus()){
          Navigator.of(context).pushReplacementNamed(PropertiesScreen.routeName);
    }else{
          Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);
    }
  }

  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      backgroundColor: AppTheme.mainAppThemeColor,
    body: new Center(
      child: Container(
        width: 200,
        height: 200,
        child: new Image.asset('assets/images/app_logo.png',
        ),
      ),
    ),
  );
  }
}
