import '../../widgets/aldar_base_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import '../../app/app_localizations.dart';
import '../../widgets/main_drawer.dart';
import '../../app/app_theme.dart';
import '../../widgets/property_list.dart';
import 'property_details.dart';
import '../filters/filter_screen.dart';
import 'package:provider/provider.dart';
import '../../provider/property_provider.dart';
import '../../model/property.dart';

class PropertiesScreen extends StatefulWidget {
  static const routeName = '/properties';
  @override
  _PropertiesScreenState createState() => _PropertiesScreenState();
}

class _PropertiesScreenState extends State<PropertiesScreen> {
  @override
  Widget build(BuildContext context) {
    final propertProvider = Provider.of<PropertProvider>(context, listen: false);
    final List<Property> _items = propertProvider.items;
    print(_items);

    return Scaffold(
        appBar: AldarBaseAppbar(title:'properties',
            appBar: AppBar(),
            widgets: <Widget>[
            IconButton(
              icon: Icon(Icons.sort),
              onPressed: () {
                Navigator.of(context).push(CupertinoPageRoute(
                    fullscreenDialog: true, builder: (context) => Filter()));
              },
            )
          ],
        ),
        drawer: HomeDrawer(),
        body: Column(
          children: <Widget>[
            Expanded(
              child: ListView.builder(
                itemCount: _items.length,
                itemBuilder: (ctx, i) {
                  return GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, PropertyDetails.routeName,
                            arguments: _items[i]);
                      },
                      child: PropertyListItem(_items[i]));
                },
              ),
            )
          ],
        ));
  }
}
