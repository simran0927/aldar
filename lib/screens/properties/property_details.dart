import '../../widgets/aldar_base_appbar.dart';
import 'package:flutter/material.dart';
import '../../app/app_localizations.dart';
import '../../app/app_theme.dart';
import '../../model/property.dart';

class PropertyDetails extends StatelessWidget {
  static const routeName = '/property_details';

  @override
  Widget build(BuildContext context) {
    final Property _item =
        ModalRoute.of(context).settings.arguments as Property;
    return Scaffold(
      appBar: AldarBaseAppbar(title:_item.titleTxt,
          title_localized:true,
          appBar: AppBar()),
      floatingActionButton: FloatingActionButton(
        backgroundColor: AppTheme.mainAppThemeColor,
        child: Icon(Icons.map),
        onPressed: (){
          
        },
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 250,
              child: Hero(
                tag: _item.id,
                child: Image.asset(
                  _item.imagePath,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              height: 100,
              width: double.infinity,
              color: AppTheme.mainAppThemeColor,
              padding: EdgeInsets.only(left: 10,top: 5, right: 5, bottom: 5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(_item.titleTxt, style: TextStyle(color: AppTheme.nearlyWhite, fontSize: 25),),
                  Text(_item.subTxt, style: TextStyle(color: AppTheme.nearlyWhite, fontSize: 15),)
                ],
              ),
            ),
            Text(
              _item.detailTxt,
              textAlign: TextAlign.left,
              ),
          ],
        ),
      ),
    );
  }
}
