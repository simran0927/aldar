import 'package:aldar_broker/datastore/database/aldar_database.dart';
import 'package:provider/provider.dart';

import '../../assets/aldar_icons.dart';
import '../../model/dashboard/sales_ranking_item.dart';
import '../../screens/dashboard/dashboard_theme.dart';
import '../../app/app_localizations.dart';
import '../../app/app_theme.dart';
import '../../utils/string_utils.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SalesRankingListItem extends StatelessWidget {

  final SalesRank _item;
  final formatter = new NumberFormat("#,###");

  SalesRankingListItem(this._item);

  @override
  Widget build(BuildContext context) {
    //final _item = Provider.of<SalesRankItem>(context, listen: false);
    return Container(
      padding: AppTheme.card_margin,
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(DashBoardTheme.sales_rank_card_curve_radius)),
        child: Stack(
          children: <Widget>[
            Container(
              height: DashBoardTheme.sales_rank_card_height,
              width: double.infinity,
              decoration: new BoxDecoration(
                color: const Color(0xff7c94b6),
                image: new DecorationImage(
                  fit: BoxFit.cover,
                  colorFilter: new ColorFilter.mode(DashBoardTheme.sales_rank_cardColorFilter, BlendMode.darken),
                  image: ExactAssetImage(_item.imagePath),
                ),
              ),
            ),Positioned(
              top: DashBoardTheme.sales_rank_card_padding_pos,
              left: DashBoardTheme.sales_rank_card_padding_pos,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: Icon(AldarIcons.award_symbol,
                      color: AppTheme.mainAppThemeColor,
                      size: 35,
                    ),
                  ),
                  Text(
                      StringUtils.formartasposition(_item.rank),
                      style: TextStyle(
                          color: AppTheme.mainAppThemeColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 15
                      )
                  )
                ],
              ),
            ),
            Positioned(
              top: DashBoardTheme.sales_rank_card_padding_pos,
              right: DashBoardTheme.sales_rank_card_padding_pos,
              child: Material(
                color: Colors.transparent,
                child: Text(
                    _item.year.toString(),
                    style: TextStyle(
                        color: AppTheme.mainAppThemeColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 18
                    )
                ),
              ),
            ),
            Positioned(
              bottom: DashBoardTheme.sales_rank_card_padding_pos,
              left: DashBoardTheme.sales_rank_card_padding_pos,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: salesCommissionWidget(context,_item),
              ),
            ),
            Positioned(
              bottom: DashBoardTheme.sales_rank_card_padding_pos,
              right: DashBoardTheme.sales_rank_card_padding_pos,
              child: titleSubTitleWidget(context,
                  formatter.format(_item.unitsSoldVal).toUpperCase(),AppTranslations.of(context)
                      .text('units_sold'),CrossAxisAlignment.end),
            )
          ],
        ),
      ),
    );
  }

  List <Widget> salesCommissionWidget(BuildContext context,SalesRank salesRankItem) {
    List <Widget> list = new List();
    list.add(titleSubTitleWidget(context,
        formatter.format(salesRankItem.salesVal).toUpperCase(),AppTranslations.of(context)
            .text('total_sales_value'),CrossAxisAlignment.start));
    list.add(SizedBox(height: 10));
    list.add(titleSubTitleWidget(context,
        formatter.format(salesRankItem.commissionVal).toUpperCase(),AppTranslations.of(context)
            .text('commission_earned'),CrossAxisAlignment.start));
    return list;
  }
  Widget titleSubTitleWidget(BuildContext context,String value,String title,CrossAxisAlignment alignment) {
    return Column(
      crossAxisAlignment: alignment,
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            value,
            style: TextStyle(
                color: AppTheme.mainAppThemeColor,
                fontWeight: FontWeight.bold,
                fontSize: 18
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: TextStyle(
              color: AppTheme.mainAppThemeColor,
            ),
          ),
        )
      ],
    );
  }
}
