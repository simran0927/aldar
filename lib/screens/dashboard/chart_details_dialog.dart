import '../../model/dashboard/piechart_item.dart';
import 'package:intl/intl.dart';

import '../../app/app_localizations.dart';
import '../../app/app_theme.dart';
import 'package:flutter/material.dart';

import 'dashboard_theme.dart';

class ChartDetailsDialog extends StatelessWidget {

  final String title;
  final List<PieChartItem> chartData;
  final formatter = new NumberFormat("#,###");

  ChartDetailsDialog({
    @required this.title,
    @required this.chartData,
  });

  @override
  Widget build(BuildContext context) {
    return Center( // Aligns the container to center
        child: Container( // A simplified version of dialog.
          width: MediaQuery.of(context).size.width *.95,
          height: MediaQuery.of(context).size.height *.8,
          decoration: BoxDecoration(color: AppTheme.mainAppThemeColor,
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(DashBoardTheme.sales_rank_card_curve_radius)),
            child: Scaffold(
              body: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                    padding: AppTheme.alert_dialog_titlebar_padding,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(color: AppTheme.mainAppThemeColor,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(20.0),topRight: Radius.circular(20.0))),
                    child: ListTile(
                      leading:
                      Icon(Icons.business, size: DashBoardTheme.card_icon_size, color: AppTheme.yellow),
                      title: Align(
                        alignment: Alignment(-1.1, 0),
                        child: Text(
                          AppTranslations.of(context)
                              .text('sales_summary')
                              .toUpperCase(),
                          style: TextStyle(
                              color: AppTheme.yellow,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      trailing: IconButton(icon: Icon(Icons.close,
                          size: DashBoardTheme.card_icon_size,
                          color: DashBoardTheme.alert_close_buttonColor,
                      ), onPressed: (){
                        Navigator.of(context).pop();
                      }) ,
                    ),
                  ),
                  AppTheme.primaryColorDivider,
                  Expanded(
                    child: ListView.separated(
                      separatorBuilder: (context, index) => AppTheme.primaryColorDivider,
                      itemCount: chartData.length,
                      itemBuilder: (ctx, i) {
                        return GestureDetector(
                            onTap: () {

                            },
                            child: ListTile(
                              contentPadding: EdgeInsets.only(left: -20, right: 20),
                              leading:Container(
                              color: chartData[i].colorVal,
                              width: 10,
                              height: 25),
                              title: Align(
                                alignment: Alignment(-1.1, 0),
                                child: Text(
                                  chartData[i].itemName,
                                  style: TextStyle(
                                      color: AppTheme.secondaryAppThemeColor,
                                  ),
                                ),
                              ),
                                trailing: Align(
                                  widthFactor: 1,
                                  alignment: Alignment(-1.1, 0),
                                  child: Text(
                                    'AED '+formatter.format(chartData[i].itemValue),
                                    style: TextStyle(
                                        color: AppTheme.secondaryAppThemeColor,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                )
                            ));
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
    );
  }
}