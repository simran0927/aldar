import '../../app/app_theme.dart';
import 'package:flutter/material.dart';

  // Styles used in the Dashboard Module specific widgets
class DashBoardTheme {
  DashBoardTheme._();

  //Colors used in Dashboard Module specific widgets

  static const Color sales_valueTitleColor = Color(0xffeb9812);
  static const Color units_soldTitleColor = Color(0xffeb9812);
  static const Color commissions_earnedTitleColor = Color(0xff51c14d);
  static const Color pending_commissionTitleColor = Color(0xffd04848);
  static Color sales_rank_cardColorFilter = Colors.black.withOpacity(0.6);
  static const Color alert_close_buttonColor = AppTheme.opaqueBlack;

  //Dimensions used in Dashboard Module specific widgets

  //Sales Performance Cards
  static const double card_icon_size = 30;
  static const double card_title_divider_height = 2;
  static const double card_title_divider_width = 120;
  static const double card_title_divider_left_margin = 20;

  static const double sales_performance_bottom_margin = 30;

  //Charts
  static const int anim_duration = 500;
  //Piechart
  static const double sales_performance_piechart_width = 200;
  static const double sales_performance_piechart_height = 200;
  static const int sales_performance_piechart_anim_duration = anim_duration;
  static const int sales_performance_piechart_arc_width = 30;
  //Barchart
  static const double sales_performance_barchart_height = 250;
  static const double sales_performance_barchart_left_padding = 10;
  static const int sales_performance_barchart_anim_duration = anim_duration;

  //Sales Rank Cards
  static const double sales_rank_card_height = 200;
  static const double sales_rank_card_padding_pos = 10;
  static const double sales_rank_card_curve_radius = 12.0;

  //Text Styles used in Dashboard Module specific widgets

  static TextStyle text_cardListItemTitle =AppTheme.text_normal.apply(
    color: AppTheme.secondaryAppThemeColorLight
  );

  static TextStyle card_title_textStyle =AppTheme.text_normal;

  //Padding, Margins, Divider and EmptySpace used in the Aldar Application

  static const EdgeInsets card_sublist_padding = const EdgeInsets.only(left: 20, right: 20);


}