
import '../../model/dashboard/barchart_item.dart';
import '../../model/dashboard/piechart_item.dart';
import '../../model/dashboard/sales_performance_dao.dart';
import '../../screens/dashboard/dashboard_theme.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../app/app_localizations.dart';
import '../../app/app_theme.dart';
import 'package:charts_flutter/flutter.dart' as charts;

import 'chart_details_dialog.dart';

class SalesPerformanceScreen extends StatefulWidget {
  static const routeName = '/sales_performance';

  @override
  _SalesPerformanceScreenState createState() => _SalesPerformanceScreenState();
}

class _SalesPerformanceScreenState extends State<SalesPerformanceScreen> {
  List<charts.Series<PieChartItem,String>> _seriesPieData;
  List<charts.Series<BarChartItem,String>> _seriesBarData;
  var _salesPreformanceData;
  var pieData;
  var barData;

  _generateData(){
    var salesValueData =[
      new SalesPreformanceItem('current_year', 398311658),
      new SalesPreformanceItem('pending_sales', 398311658),
      new SalesPreformanceItem('approved_sales', 398311658)
    ];
    var unitsSoldData =[
      new SalesPreformanceItem('over_all', 398311658),
      new SalesPreformanceItem('current_year', 398311658)
    ];
    var commissionEarnedData =[
      new SalesPreformanceItem('over_all', 398311658),
      new SalesPreformanceItem('current_year', 398311658)
    ];
    var pendingCommissionData =[
      new SalesPreformanceItem('over_all', 398311658),
      new SalesPreformanceItem('current_year', 398311658)
    ];

    _salesPreformanceData = {'sales_value':new SalesPreformance('sales_value',DashBoardTheme.sales_valueTitleColor,salesValueData),
      'units_sold':new SalesPreformance('units_sold',DashBoardTheme.units_soldTitleColor,unitsSoldData),
      'commissions_earned':new SalesPreformance('commissions_earned',DashBoardTheme.commissions_earnedTitleColor,commissionEarnedData),
      'pending_commission':new SalesPreformance('pending_commission',DashBoardTheme.pending_commissionTitleColor,pendingCommissionData)};

    pieData = [
      new PieChartItem("Al Reeman", 134896042, Color(0xffe4c14a)),
      new PieChartItem("Yas Acres", 254896043, Color(0xffde9742)),
      new PieChartItem("West Yas", 354896043, Color(0xffd96e3b)),
      new PieChartItem("Al Muneera", 419896043, Color(0xffd34534)),
      new PieChartItem("Al Murjan", 244896043, Color(0xffbe3b45)),
      new PieChartItem("Al Oyoun Village",3330896043, Color(0xff9a5270)),
      new PieChartItem("Mayan", 254896043, Color(0xff75699b)),
      new PieChartItem("Meera", 124896043, Color(0xff517fc5)),
      new PieChartItem("Nareel Island",2254896043, Color(0xff2d96f0)),
      new PieChartItem("Al Ghadeer",2354896043, Color(0xff37a3c5))
    ];

    barData = [
      new BarChartItem("Jan", 39, Color(0xffcac05f)),
      new BarChartItem("Feb", 69, Color(0xffcac05f)),
      new BarChartItem("Mar", 79, Color(0xffcac05f)),
      new BarChartItem("Apr", 29, Color(0xffcac05f)),
      new BarChartItem("May", 59, Color(0xffcac05f)),
      new BarChartItem("Jun", 99, Color(0xffcac05f)),
      new BarChartItem("Jul", 89, Color(0xffcac05f)),
      new BarChartItem("Aug", 39, Color(0xffcac05f)),
      new BarChartItem("Sep", 19, Color(0xffcac05f)),
      new BarChartItem("Oct", 79, Color(0xffcac05f)),
      new BarChartItem("Nov", 59, Color(0xffcac05f)),
      new BarChartItem("Dec", 49, Color(0xffcac05f)),
    ];

    _seriesPieData.add(
      charts.Series(
          data:pieData,
          domainFn: (PieChartItem task,_) => task.itemName,
          measureFn: (PieChartItem task, _) => task.itemValue,
          colorFn: (PieChartItem task,_)=> charts.ColorUtil.fromDartColor(task.colorVal),
          id:'Sales',
          labelAccessorFn: (PieChartItem row, _)=>'${row.itemValue}'
      ),
    );

    _seriesBarData.add(
      charts.Series(
          data:barData,
          domainFn: (BarChartItem task,_) => task.itemName,
          measureFn: (BarChartItem task, _) => task.itemValue,
          colorFn: (BarChartItem task,_)=> charts.ColorUtil.fromDartColor(task.colorVal),
          id:'Sales Summary',
          labelAccessorFn: (BarChartItem row, _)=>'${row.itemValue}'
      ),
    );
  }
//Initialize and Populates Data for Sales Performance View
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _seriesPieData = List<charts.Series<PieChartItem,String>>();
    _seriesBarData = List<charts.Series<BarChartItem,String>>();
    _generateData();
  }
//Generate Sales Performance ListView
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: AppTheme.main_block_padding,
        margin: EdgeInsets.only(bottom: DashBoardTheme.sales_performance_bottom_margin),
        color: AppTheme.pageBackground,
        child: Column(
          children: <Widget>[
            Card(
              margin: AppTheme.card_margin,
              child: Column(
                children: <Widget>[
                  pieChartCardWidget(context,'sales_value'),
                  AppTheme.primaryColorDivider,
                  pieChartCardWidget(context,'units_sold')
                ],
              ),
            ),
            Card(
              margin: AppTheme.card_margin,
              child: Column(
                children: <Widget>[
                  pieChartCardWidget(context,'commissions_earned'),
                  AppTheme.primaryColorDivider,
                  pieChartCardWidget(context,'pending_commission')
                ],
              ),
            ),
            Card(
              margin: AppTheme.card_margin,
              child: Column(
                children: <Widget>[
                  salesSummaryCardWidget(context)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
//Generate Sales Performance Sales Value, UnitSold, Commission Earned, Pending Commission CardView Content with PieChart
  Widget pieChartCardWidget(BuildContext context,String cardId) {

    SalesPreformance salesPreformanceItem=  _salesPreformanceData[cardId];

    return Column(
      children: <Widget>[
        ListTile(
          leading:
          Icon(Icons.business, size: DashBoardTheme.card_icon_size, color: salesPreformanceItem.colorVal),
          title: Align(
            alignment: Alignment(-1.1, 0),
            child: Text(
              AppTranslations.of(context)
                  .text(cardId)
                  .toUpperCase(),
              style: DashBoardTheme.card_title_textStyle.apply(color: salesPreformanceItem.colorVal),
            ),
          ),
        ),
        Align(
          alignment: Alignment(-1, 0),
          child: new Container(
              color: salesPreformanceItem.colorVal,
              margin: const EdgeInsets.only(left: DashBoardTheme.card_title_divider_left_margin),
              width: DashBoardTheme.card_title_divider_width,
              height: DashBoardTheme.card_title_divider_height),
        ),
        Row(
          children: <Widget>[
            Container(
                padding: DashBoardTheme.card_sublist_padding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: cardItemsList(salesPreformanceItem.salesPerforamceItemList),
                )
            ),
            Align(
                alignment: Alignment.centerLeft,
                child: generatePieChart()),
          ],
        ),
      ],
    );
  }

 //Generate Sales Performance PiechartCard Sublist for DataView
  List <Widget> cardItemsList(List<SalesPreformanceItem> salesPerforamceItemList) {
    List <Widget> list = new List();
      for (var i = 0; i <= salesPerforamceItemList.length-1; i++) {
        SalesPreformanceItem item =salesPerforamceItemList[i];
        final formatter = new NumberFormat("#,###");

        list.add(titleSubTitleWidget(context,item.itemName,formatter.format(item.itemValue)));
        if(i<salesPerforamceItemList.length-1){
        list.add(AppTheme.emptyBoxSpace);
        }
    }
    return list;
  }
 //Generate Sales Performance Sales Summary CardView Content with Barchart
  Widget salesSummaryCardWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20),
      child: Column(
        children: <Widget>[
          ListTile(
            leading:
            Icon(Icons.business, size: DashBoardTheme.card_icon_size, color: AppTheme.yellow),
            title: Align(
              alignment: Alignment(-1.1, 0),
              child: Text(
                AppTranslations.of(context)
                    .text('sales_summary')
                    .toUpperCase(),
                style: TextStyle(
                  color: AppTheme.yellow,
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment(-1, 0),
            child: new Container(
                color: AppTheme.yellow,
                margin: const EdgeInsets.only(left: DashBoardTheme.card_title_divider_left_margin),
                width: DashBoardTheme.card_title_divider_width,
                height: DashBoardTheme.card_title_divider_height),
          ),
          generateBarChart(),
        ],
      ),
    );
  }

  //Generate Sales Performance PiechartCard Sublist Items for DataView
  Widget titleSubTitleWidget(BuildContext context,String title_id,String value) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            AppTranslations.of(context)
                .text(title_id),
            style: DashBoardTheme.text_cardListItemTitle,
          ),
        ),
        SizedBox(
          height: 5,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            value,
            style: AppTheme.text_mediumBold,
          ),
        )
      ],
    );
  }

  //Generate Sales Performance Piechart View
  Widget generatePieChart() {
    return GestureDetector(
      onTap: (){
        _showDialog();
      },
      child: Container(
        width:  DashBoardTheme.sales_performance_piechart_width,
        height: DashBoardTheme.sales_performance_piechart_height,
        child: charts.PieChart(
          _seriesPieData,
          animate: true,
          animationDuration: Duration(milliseconds: DashBoardTheme.sales_performance_piechart_anim_duration),
          defaultInteractions: false,
          defaultRenderer: new charts.ArcRendererConfig(
              arcWidth: DashBoardTheme.sales_performance_piechart_arc_width,
              arcRendererDecorators: [
                new charts.ArcLabelDecorator(
                    labelPosition: charts.ArcLabelPosition.inside
                )
              ]
          ),
        ),
      ),
    );
  }

  // Shows Chart Details Dialog View for Piechart
  void _showDialog() {
    showDialog(
        context: context,
        builder: (_) => ChartDetailsDialog(title: "Sales Value",chartData: pieData,)
    );
  }


  //Generate Sales Performance Barchart View
  Widget generateBarChart() {
    return Container(
      padding: EdgeInsets.only(left: DashBoardTheme.sales_performance_barchart_left_padding),
      width:  double.infinity,
      height: DashBoardTheme.sales_performance_barchart_height,
      child: charts.BarChart(
          _seriesBarData,
        animate: true,
        animationDuration: Duration(milliseconds: DashBoardTheme.sales_performance_barchart_anim_duration),
          behaviors: [
            new charts.ChartTitle('Sales value',
                behaviorPosition: charts.BehaviorPosition.start,
                titleOutsideJustification:
                charts.OutsideJustification.middleDrawArea),
        new charts.ChartTitle('Month value',
            behaviorPosition: charts.BehaviorPosition.bottom,
            titleOutsideJustification:
            charts.OutsideJustification.middleDrawArea)
        ],
          primaryMeasureAxis: new charts.NumericAxisSpec(
              tickProviderSpec:
              new charts.BasicNumericTickProviderSpec(desiredTickCount: 0))
      ),
    );
  }


}


