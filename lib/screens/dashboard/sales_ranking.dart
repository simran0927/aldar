import 'package:aldar_broker/datastore/database/aldar_database.dart';
import 'package:aldar_broker/datastore/database/dashboard/sales_rank_dao.dart';

import '../../provider/dashboard/sales_ranking_provider.dart';
import 'package:provider/provider.dart';
import '../../services/dashboard/sales_ranking_service.dart';

import '../../model/dashboard/sales_ranking_item.dart';
import '../../widgets/aldar_base_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../app/app_localizations.dart';
import '../../app/app_theme.dart';
import '../../widgets/main_drawer.dart';
import 'sales_ranking_list_item.dart';

class SalesRankingScreen extends StatefulWidget {
  static const routeName = '/sales_ranking';

  @override
  _SalesRankingScreenState createState() => _SalesRankingScreenState();
}

class _SalesRankingScreenState extends State<SalesRankingScreen> {
  var _isLoading = false;
  var _isInit = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  /*@override
  void didChangeDependencies() {
    if (_isInit) {
      setState(() {
        _isLoading = true;
      });
      Provider.of<SalesRankingProvider>(context).fetchAndSetSalesRanks().then((_) {
        setState(() {
          _isLoading = false;
        });
      });
    }
    _isInit = false;
    super.didChangeDependencies();
  }*/

  Future<List<SalesRankItem>> getSalesRankData(){
    return SalesRankingService().getSalesRanking();
  }


  @override
  Widget build(BuildContext context) {
    final dao=Provider.of<SalesRankDao>(context);
    return Container(
      color: AppTheme.pageBackground,
      padding: AppTheme.main_block_padding,
      child: _isLoading
          ? Center(child: CircularProgressIndicator(),)
          :FutureBuilder<List<SalesRank>>(
          future: dao.getSalesRanks(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (ctx, i) {
                  return GestureDetector(
                      onTap: () {

                      },
                      child: SalesRankingListItem(snapshot.data[i]));
                },
              );
            } else {
              return Center(child: CircularProgressIndicator(),);
            }
          }),
    );
  }

/*      ListView rankListView() {
        final sales_rank_data = Provider.of<SalesRankingProvider>(context);
        final dao=Provider.of<SalesRankDao>(context);
        dao.getSalesRanks().then((result) {
          List<SalesRank> _salesRankData =result as List<SalesRank>;
          print(_salesRankData.length.toString());
        });
        final sales_rank_items=sales_rank_data.items;
        return  ListView.builder(
          itemCount: sales_rank_items.length,
          itemBuilder: (ctx, i) {
            return GestureDetector(
                  onTap: () {

                  },
                  child: SalesRankingListItem(sales_rank_items[i]));
          },
        );
      }*/
}


