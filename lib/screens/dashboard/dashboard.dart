import 'package:aldar_broker/screens/common/pdf.dart';
import 'package:aldar_broker/services/utility/pdfService.dart';
import 'package:aldar_broker/widgets/aldar_base_appbar.dart';

import '../../screens/filters/filter_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../../app/app_localizations.dart';
import '../../app/app_theme.dart';
import '../../widgets/main_drawer.dart';
import 'sales_performance.dart';
import 'sales_ranking.dart';

class DashboardScreen extends StatefulWidget {
  static const routeName = '/dashboard';
  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        // The number of tabs / content sections to display.
        length: 2,
        child: Scaffold(
          appBar: AldarBaseAppbar(
              title: 'dashboard',
              appBar: AppBar(),
              widgets: <Widget>[
                IconButton(
                  icon: Icon(Icons.picture_as_pdf),
                  onPressed: () {
                    PDFService()
                        .createFileOfPdfUrl(
                            "https://www.adobe.com/content/dam/acom/en/devnet/acrobat/pdfs/pdf_open_parameters.pdf")
                        .then((f) {
                      setState(() {
                        print(f.path);
                        Navigator.of(context).push(CupertinoPageRoute(
                            fullscreenDialog: true,
                            builder: (context) => PDFScreen(f.path)));
                      });
                    });
                  },
                ),
                IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    Navigator.of(context).push(CupertinoPageRoute(
                        fullscreenDialog: true,
                        builder: (context) => Filter()));
                  },
                )
              ],
              bottom: TabBar(
                indicatorColor: AppTheme.tabIndicatorColor,
                unselectedLabelColor: AppTheme.tabunselectedLabelColor,
                tabs: [
                  Tab(
                      child: Text(
                          AppTranslations.of(context).text("sales_performance"),
                          style: AppTheme.tabtitle)),
                  Tab(
                      child: Text(
                          AppTranslations.of(context).text("sales_ranking"),
                          style: AppTheme.tabtitle))
                ],
              )),
          drawer: HomeDrawer(),
          body: TabBarView(
            children: [
              SalesPerformanceScreen(),
              SalesRankingScreen(),
            ],
          ),
        ));
  }
}
