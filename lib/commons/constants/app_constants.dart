
class AppConstants {
  AppConstants._();

  // base url
  static const String baseUrl = "http://demo1538336.mockable.io/";

  // receiveTimeout
  static const int receiveTimeout = 5000;

  // connectTimeout
  static const int connectionTimeout = 3000;

  // booking endpoints
  static const String GET_SALES_RANKING_API = "/sales_ranking";
}