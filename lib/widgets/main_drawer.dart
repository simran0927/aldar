import '../screens/dashboard/dashboard.dart';
import '../screens/properties/properties.dart';
import 'package:provider/provider.dart';
import '../app/app_theme.dart';
import 'package:flutter/material.dart';
import '../screens/support/help_screen.dart';
import '../screens/support/feedback_screen.dart';
import '../app/app_localizations.dart';
import '../screens/settings/settings_screen.dart';
import '../provider/drawer_state.dart';
import '../screens/social/invite_friends.dart';
import '../screens/support/about_us.dart';
import '../screens/social/rate_app.dart';
import '../screens/auth/login_screen.dart';
import '../provider/auth/auth_provider.dart';

class HomeDrawer extends StatefulWidget {
  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  List<DrawerList> drawerList;
  var currentSelectedIndex = DrawerIndex.Dashboard;
  @override
  void initState() {
    super.initState();
  }

  void doSignOut(){
    final authProvider = Provider.of<AuthProvider>(context, listen: false);
    authProvider.updateLoggedInsStatus(false);
    Navigator.of(context).pushReplacementNamed(LoginScreen.routeName);

  }

  void setdDrawerListArray() {
    drawerList = <DrawerList>[
      DrawerList(
        index: DrawerIndex.Dashboard,
        labelName: AppTranslations.of(context).text('dashboard'),
        icon: Icon(Icons.dashboard),
      ),
      DrawerList(
        index: DrawerIndex.Properties,
        labelName: AppTranslations.of(context).text('properties'),
        icon: Icon(Icons.business),
      ),
      DrawerList(
        index: DrawerIndex.Help,
        labelName: AppTranslations.of(context).text('help'),
        icon: Icon(Icons.help),
      ),
      DrawerList(
        index: DrawerIndex.FeedBack,
        labelName: AppTranslations.of(context).text('feedback'),
        icon: Icon(Icons.feedback),
      ),
      DrawerList(
        index: DrawerIndex.Invite,
        labelName: AppTranslations.of(context).text('invite_friend'),
        icon: Icon(Icons.group),
      ),
      DrawerList(
        index: DrawerIndex.Share,
        labelName: AppTranslations.of(context).text('rate_app'),
        icon: Icon(Icons.share),
      ),
      DrawerList(
        index: DrawerIndex.About,
        labelName: AppTranslations.of(context).text('about_us'),
        icon: Icon(Icons.info),
      ),
      DrawerList(
        index: DrawerIndex.Settings,
        labelName: AppTranslations.of(context).text('settings'),
        icon: Icon(Icons.settings),
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    setdDrawerListArray();
    final drawerState = Provider.of<DrawerState>(context, listen: false);
    setState(() {
      this.currentSelectedIndex = drawerState.getCurrentDrawerIndex();
      print(this.currentSelectedIndex);
    });
    return Drawer(
      child: Container(
        color: AppTheme.secondaryAppThemeColor,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              color: AppTheme.mainAppThemeColor,
              padding: const EdgeInsets.only(top: 40.0),
              child: Container(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 120,
                      width: 120,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: AppTheme.grey.withOpacity(0.6),
                              offset: const Offset(2.0, 4.0),
                              blurRadius: 8),
                        ],
                      ),
                      child: ClipRRect(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(60.0)),
                        child: Image.asset('assets/images/userImage.png'),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8, left: 4),
                      child: Text(
                        'Chris Hemsworth',
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: AppTheme.white,
                          fontSize: 22,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Divider(
              height: 1,
              color: AppTheme.grey.withOpacity(0.6),
            ),
            Expanded(
              child: ListView.builder(
                physics: const BouncingScrollPhysics(),
                padding: const EdgeInsets.all(0.0),
                itemCount: drawerList.length,
                itemBuilder: (BuildContext context, int index) {
                  return DrawerItemView(drawerList[index]);
                },
              ),
            ),
            Divider(
              height: 1,
              color: AppTheme.grey.withOpacity(0.6),
            ),
            Column(
              children: <Widget>[
                ListTile(
                  title: Text(
                    AppTranslations.of(context).text('sign_out'),
                    style: TextStyle(
                      fontFamily: AppTheme.fontName,
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                      color: AppTheme.mainAppThemeColor,
                    ),
                    textAlign: TextAlign.left,
                  ),
                  trailing: Icon(
                    Icons.power_settings_new,
                    color: AppTheme.mainAppThemeColor,
                  ),
                  onTap: () {
                    doSignOut();
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).padding.bottom,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget DrawerItemView(DrawerList listData) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Colors.grey.withOpacity(0.1),
        highlightColor: Colors.transparent,
        onTap: () {
          navigationtoScreen(listData.index);
        },
        child: Stack(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Row(
                children: <Widget>[
                  Container(
                    width: 6.0,
                    height: 46.0,
                  ),
                  const Padding(
                    padding: EdgeInsets.all(4.0),
                  ),
                  listData.isAssetsImage
                      ? Container(
                          width: 24,
                          height: 24,
                          child: Image.asset(listData.imageName,
                              color: Colors.blue),
                        )
                      : Icon(listData.icon.icon,
                          color: AppTheme.nearlyWhite),
                  const Padding(
                    padding: EdgeInsets.all(4.0),
                  ),
                  Text(
                    listData.labelName,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color:AppTheme.nearlyWhite,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ],
              ),
            ),
            this.currentSelectedIndex == listData.index
                ? Padding(
                    padding: EdgeInsets.only(top: 8, bottom: 8),
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.75 - 64,
                      height: 46,
                      decoration: BoxDecoration(
                        color: AppTheme.mainAppThemeColor.withOpacity(0.4),
                        borderRadius: new BorderRadius.only(
                          topLeft: Radius.circular(0),
                          topRight: Radius.circular(28),
                          bottomLeft: Radius.circular(0),
                          bottomRight: Radius.circular(28),
                        ),
                      ),
                    ),
                  )
                : const SizedBox()
          ],
        ),
      ),
    );
  }

  Future<void> navigationtoScreen(DrawerIndex indexScreen) async {
    final drawerState = Provider.of<DrawerState>(context, listen: false);
    drawerState.setDrawerIndex(indexScreen);
    switch (indexScreen) {
      case DrawerIndex.Dashboard:
        Navigator.pushReplacementNamed(context, DashboardScreen.routeName);
        break;
      case DrawerIndex.Properties:
        Navigator.pushReplacementNamed(context, PropertiesScreen.routeName);
        break;
      case DrawerIndex.Help:
        Navigator.pushReplacementNamed(context, HelpScreen.routeName);
        break;
      case DrawerIndex.FeedBack:
        Navigator.pushReplacementNamed(context, FeedbackScreen.routeName);
        break;
      case DrawerIndex.Settings:
        Navigator.pushReplacementNamed(context, SettingsScreen.routeName);
        break;
      case DrawerIndex.Invite:
        Navigator.pushReplacementNamed(context, InviteFriends.routeName);
        break;
      case DrawerIndex.About:
        Navigator.pushReplacementNamed(context, AboutUS.routeName);
        break;
      case DrawerIndex.Share:
        Navigator.pushReplacementNamed(context, RateApp.routeName);
        break;
        

      default:
    }
  }
}

enum DrawerIndex {
  Dashboard,
  Properties,
  FeedBack,
  Help,
  Share,
  About,
  Invite,
  Testing,
  Settings,
}

class DrawerList {
  DrawerList({
    this.isAssetsImage = false,
    this.labelName = '',
    this.icon,
    this.index,
    this.imageName = '',
  });

  String labelName;
  Icon icon;
  bool isAssetsImage;
  String imageName;
  DrawerIndex index;
}
