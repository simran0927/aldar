import '../app/app_localizations.dart';
import 'package:flutter/material.dart';
import '../app/app_theme.dart';
import 'package:flutter/material.dart';

class AldarBaseAppbar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool title_localized;
  final AppBar appBar;
  final List<Widget> widgets;
  final PreferredSizeWidget bottom;
  /// you can add more fields that meet your needs

  const AldarBaseAppbar({Key key, this.title,this.title_localized = false, this.appBar, this.widgets,this.bottom})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
        title: Text(
            !title_localized? AppTranslations.of(context).text(title).toUpperCase() : title,
            style: AppTheme.title
//          TextStyle(
//            color: AppTheme.secondaryAppThemeColor,
//          )
        ),
        iconTheme: IconThemeData(
            color: AppTheme.secondaryAppThemeColor
        ),
        backgroundColor: AppTheme.mainAppThemeColor,
        actions: widgets,
        bottom:bottom
    );
  }

//  @override
//  Size get preferredSize => new Size.fromHeight(appBar.preferredSize.height);

  @override
  // TODO: implement preferredSize
  Size get preferredSize => bottom != null?new Size.fromHeight(appBar.preferredSize.height*2):new Size.fromHeight(appBar.preferredSize.height);
}