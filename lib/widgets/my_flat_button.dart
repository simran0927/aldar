import 'package:flutter/material.dart';
import '../app/app_theme.dart';

class MyFlatButton extends StatelessWidget {

  final Function _onPressAction;
  final String _title;

  MyFlatButton(this._onPressAction, this._title);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(
        this._title,
        style: TextStyle(
          color: AppTheme.nearlyBlack,
          fontSize: 18,
          //to make underline
          // decoration: TextDecoration.underline
          ),
      ),
      onPressed: this._onPressAction,
    );
  }
}