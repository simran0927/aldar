import 'package:flutter/material.dart';
import '../app/app_theme.dart';
class RoundedCornerButton extends StatelessWidget {
  final Function _onPressAction;
  final String _title;

  RoundedCornerButton(this._onPressAction, this._title);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(child: Text(_title, style: TextStyle(fontSize: 18),),
                onPressed: _onPressAction,
                color: AppTheme.secondaryAppThemeColor,
                textColor: AppTheme.white,
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                shape: RoundedRectangleBorder(
                  borderRadius:BorderRadius.circular(60.0) ),
              );
  }
}