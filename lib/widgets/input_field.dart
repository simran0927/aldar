import 'package:flutter/material.dart';
import '../app/app_theme.dart';

class RoundedCornerInput extends StatelessWidget {
  final String hintText;
  final bool isSecureInput;

  RoundedCornerInput({this.hintText,this.isSecureInput});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: TextField(
        obscureText: isSecureInput,
        decoration: InputDecoration(
          labelStyle: TextStyle(color: AppTheme.nearlyBlack),
          labelText: this.hintText,
          hintStyle: TextStyle(color: AppTheme.nearlyBlack),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppTheme.nearlyBlack),
              borderRadius: BorderRadius.circular(25.7),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: AppTheme.nearlyBlack),
              borderRadius: BorderRadius.circular(25.7),
            ),
            ),
        style: TextStyle(
          fontSize: 18,
          color: AppTheme.nearlyBlack),
      ),
    );
  }
}