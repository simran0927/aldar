import 'package:flutter/material.dart';
import '../app/app_theme.dart';
import '../model/property.dart';

class PropertyListItem extends StatelessWidget {

 final Property _item;

  PropertyListItem(this._item);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      color: AppTheme.pageBackground,
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(16.0)),
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Hero(
                    tag: _item.id,
                    child: Container(
                    height: 250,
                    width: double.infinity,
                    //aspectRatio: 2,
                    child: Image.asset(
                      _item.imagePath,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  color: AppTheme.mainAppThemeColor,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 16, top: 8, bottom: 8),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _item.titleTxt,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                    color: AppTheme.secondaryAppThemeColor,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 22,
                                  ),
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      _item.subTxt,
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: AppTheme.nearlyBlack),
                                    ),
                                    const SizedBox(
                                      width: 4,
                                    ),
                                    Icon(
                                      Icons.home,
                                      size: 12,
                                      color: AppTheme.nearlyBlack,
                                    ),
                                    Expanded(
                                      child: Text(
                                        '${_item.squareFeet} Sq.ft',
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontSize: 14,
                                            color:
                                                AppTheme.nearlyBlack),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 16, top: 8),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Text(
                              'AED ${_item.price}',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                color: AppTheme.mainAppThemeColor,
                                fontWeight: FontWeight.w600,
                                fontSize: 22,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Positioned(
              top: 8,
              right: 8,
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(32.0),
                  ),
                  onTap: () {},
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.favorite_border,
                      color: AppTheme.mainAppThemeColor,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
