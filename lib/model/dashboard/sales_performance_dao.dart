import 'dart:ui';

class SalesPreformance {
  String itemName;
  Color colorVal;
  List<SalesPreformanceItem> salesPerforamceItemList;
  SalesPreformance(this.itemName,this.colorVal,this.salesPerforamceItemList);
}

class SalesPreformanceItem {
  String itemName;
  double itemValue;
  SalesPreformanceItem(this.itemName,this.itemValue);
}