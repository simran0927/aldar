import 'dart:ui';

class PieChartItem {
  String itemName;
  double itemValue;
  Color colorVal;

  PieChartItem(this.itemName,this.itemValue,this.colorVal);
}