import 'dart:ui';

class BarChartItem {
  String itemName;
  double itemValue;
  Color colorVal;

  BarChartItem(this.itemName,this.itemValue,this.colorVal);
}