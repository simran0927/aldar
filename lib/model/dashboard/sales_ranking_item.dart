import 'package:flutter/foundation.dart';

class SalesRankItem extends ChangeNotifier{

  int id;
  String imagePath;
  int rank;
  double salesVal;
  double commissionVal;
  int unitsSoldVal;
  int year;

  SalesRankItem({
    @required this.id,
    @required this.imagePath,
    @required this.rank,
    @required this.salesVal,
    @required this.commissionVal,
    @required this.unitsSoldVal,
    @required this.year
  });

  factory SalesRankItem.fromJson(Map<String, dynamic> json) {
    return SalesRankItem(
      id: json['id'] as int,
      imagePath: json['imagePath'] as String,
      rank: json['rank'] as int,
      salesVal: json['salesVal'].toDouble(),
      commissionVal: json['commissionVal'].toDouble(),
      unitsSoldVal: json['unitsSoldVal'] as int,
      year: json['year'] as int,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['imagePath'] = this.imagePath;
    data['rank'] = this.rank;
    data['salesVal'] = this.salesVal;
    data['commissionVal'] = this.commissionVal;
    data['unitsSoldVal'] = this.unitsSoldVal;
    data['year'] = this.year;
    return data;
  }

}