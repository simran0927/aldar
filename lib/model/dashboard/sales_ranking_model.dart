import 'package:flutter/foundation.dart';

import 'sales_ranking_item.dart';

class SalesRankModel {

  String last_updated_date;
  List<SalesRankItem> sales_rank_items;

  SalesRankModel({
    @required this.last_updated_date,
    @required this.sales_rank_items,
  });

  factory SalesRankModel.fromJson(Map<String, dynamic> json) {
    try{
    return SalesRankModel(
      last_updated_date: json['last_updated_date'] as String,
      sales_rank_items: json['sales_rank_items'] != null
          ? (json['sales_rank_items'] as List).map((i) => SalesRankItem.fromJson(i)).toList()
          : null,
    );
    } catch (e) {
      print(e.toString());
      throw e;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['last_updated_date'] = this.last_updated_date;
    if (this.sales_rank_items != null) {
      data['sales_rank_items'] = this.sales_rank_items.map((v) => v.toJson()).toList();
    }
    return data;
  }

}