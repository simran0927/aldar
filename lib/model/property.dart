import 'package:flutter/cupertino.dart';

class Property {
  int id;
  String imagePath;
  String titleTxt;
  String subTxt;
  String detailTxt;
  double squareFeet;
  int bed;
  int price;

  Property({
    @required this.id,
    @required this.imagePath,
    @required this.titleTxt,
    @required this.subTxt,
    @required this.detailTxt,
    @required this.squareFeet,
    @required this.bed,
    @required this.price 
  });

}