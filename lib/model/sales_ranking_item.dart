

import 'package:flutter/foundation.dart';

class SalesRankItem {

  int id;
  String imagePath;
  int rank;
  double salesVal;
  double commissionVal;
  int unitsSoldVal;
  int year;

  SalesRankItem({
    @required this.id,
    @required this.imagePath,
    @required this.rank,
    @required this.salesVal,
    @required this.commissionVal,
    @required this.unitsSoldVal,
    @required this.year
  });

}