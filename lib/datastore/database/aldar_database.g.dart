// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'aldar_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps
class SalesRank extends DataClass implements Insertable<SalesRank> {
  final int id;
  final String imagePath;
  final int rank;
  final double salesVal;
  final double commissionVal;
  final int unitsSoldVal;
  final int year;
  SalesRank(
      {@required this.id,
      @required this.imagePath,
      @required this.rank,
      @required this.salesVal,
      @required this.commissionVal,
      @required this.unitsSoldVal,
      @required this.year});
  factory SalesRank.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final doubleType = db.typeSystem.forDartType<double>();
    return SalesRank(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      imagePath: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}image_path']),
      rank: intType.mapFromDatabaseResponse(data['${effectivePrefix}rank']),
      salesVal: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}sales_val']),
      commissionVal: doubleType
          .mapFromDatabaseResponse(data['${effectivePrefix}commission_val']),
      unitsSoldVal: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}units_sold_val']),
      year: intType.mapFromDatabaseResponse(data['${effectivePrefix}year']),
    );
  }
  factory SalesRank.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return SalesRank(
      id: serializer.fromJson<int>(json['id']),
      imagePath: serializer.fromJson<String>(json['imagePath']),
      rank: serializer.fromJson<int>(json['rank']),
      salesVal: serializer.fromJson<double>(json['salesVal']),
      commissionVal: serializer.fromJson<double>(json['commissionVal']),
      unitsSoldVal: serializer.fromJson<int>(json['unitsSoldVal']),
      year: serializer.fromJson<int>(json['year']),
    );
  }
  @override
  Map<String, dynamic> toJson(
      {ValueSerializer serializer = const ValueSerializer.defaults()}) {
    return {
      'id': serializer.toJson<int>(id),
      'imagePath': serializer.toJson<String>(imagePath),
      'rank': serializer.toJson<int>(rank),
      'salesVal': serializer.toJson<double>(salesVal),
      'commissionVal': serializer.toJson<double>(commissionVal),
      'unitsSoldVal': serializer.toJson<int>(unitsSoldVal),
      'year': serializer.toJson<int>(year),
    };
  }

  @override
  T createCompanion<T extends UpdateCompanion<SalesRank>>(bool nullToAbsent) {
    return SalesRanksCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      imagePath: imagePath == null && nullToAbsent
          ? const Value.absent()
          : Value(imagePath),
      rank: rank == null && nullToAbsent ? const Value.absent() : Value(rank),
      salesVal: salesVal == null && nullToAbsent
          ? const Value.absent()
          : Value(salesVal),
      commissionVal: commissionVal == null && nullToAbsent
          ? const Value.absent()
          : Value(commissionVal),
      unitsSoldVal: unitsSoldVal == null && nullToAbsent
          ? const Value.absent()
          : Value(unitsSoldVal),
      year: year == null && nullToAbsent ? const Value.absent() : Value(year),
    ) as T;
  }

  SalesRank copyWith(
          {int id,
          String imagePath,
          int rank,
          double salesVal,
          double commissionVal,
          int unitsSoldVal,
          int year}) =>
      SalesRank(
        id: id ?? this.id,
        imagePath: imagePath ?? this.imagePath,
        rank: rank ?? this.rank,
        salesVal: salesVal ?? this.salesVal,
        commissionVal: commissionVal ?? this.commissionVal,
        unitsSoldVal: unitsSoldVal ?? this.unitsSoldVal,
        year: year ?? this.year,
      );
  @override
  String toString() {
    return (StringBuffer('SalesRank(')
          ..write('id: $id, ')
          ..write('imagePath: $imagePath, ')
          ..write('rank: $rank, ')
          ..write('salesVal: $salesVal, ')
          ..write('commissionVal: $commissionVal, ')
          ..write('unitsSoldVal: $unitsSoldVal, ')
          ..write('year: $year')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          imagePath.hashCode,
          $mrjc(
              rank.hashCode,
              $mrjc(
                  salesVal.hashCode,
                  $mrjc(commissionVal.hashCode,
                      $mrjc(unitsSoldVal.hashCode, year.hashCode)))))));
  @override
  bool operator ==(other) =>
      identical(this, other) ||
      (other is SalesRank &&
          other.id == id &&
          other.imagePath == imagePath &&
          other.rank == rank &&
          other.salesVal == salesVal &&
          other.commissionVal == commissionVal &&
          other.unitsSoldVal == unitsSoldVal &&
          other.year == year);
}

class SalesRanksCompanion extends UpdateCompanion<SalesRank> {
  final Value<int> id;
  final Value<String> imagePath;
  final Value<int> rank;
  final Value<double> salesVal;
  final Value<double> commissionVal;
  final Value<int> unitsSoldVal;
  final Value<int> year;
  const SalesRanksCompanion({
    this.id = const Value.absent(),
    this.imagePath = const Value.absent(),
    this.rank = const Value.absent(),
    this.salesVal = const Value.absent(),
    this.commissionVal = const Value.absent(),
    this.unitsSoldVal = const Value.absent(),
    this.year = const Value.absent(),
  });
  SalesRanksCompanion copyWith(
      {Value<int> id,
      Value<String> imagePath,
      Value<int> rank,
      Value<double> salesVal,
      Value<double> commissionVal,
      Value<int> unitsSoldVal,
      Value<int> year}) {
    return SalesRanksCompanion(
      id: id ?? this.id,
      imagePath: imagePath ?? this.imagePath,
      rank: rank ?? this.rank,
      salesVal: salesVal ?? this.salesVal,
      commissionVal: commissionVal ?? this.commissionVal,
      unitsSoldVal: unitsSoldVal ?? this.unitsSoldVal,
      year: year ?? this.year,
    );
  }
}

class $SalesRanksTable extends SalesRanks
    with TableInfo<$SalesRanksTable, SalesRank> {
  final GeneratedDatabase _db;
  final String _alias;
  $SalesRanksTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _imagePathMeta = const VerificationMeta('imagePath');
  GeneratedTextColumn _imagePath;
  @override
  GeneratedTextColumn get imagePath => _imagePath ??= _constructImagePath();
  GeneratedTextColumn _constructImagePath() {
    return GeneratedTextColumn('image_path', $tableName, false,
        minTextLength: 1, maxTextLength: 50);
  }

  final VerificationMeta _rankMeta = const VerificationMeta('rank');
  GeneratedIntColumn _rank;
  @override
  GeneratedIntColumn get rank => _rank ??= _constructRank();
  GeneratedIntColumn _constructRank() {
    return GeneratedIntColumn('rank', $tableName, false,
        defaultValue: Constant(0));
  }

  final VerificationMeta _salesValMeta = const VerificationMeta('salesVal');
  GeneratedRealColumn _salesVal;
  @override
  GeneratedRealColumn get salesVal => _salesVal ??= _constructSalesVal();
  GeneratedRealColumn _constructSalesVal() {
    return GeneratedRealColumn('sales_val', $tableName, false,
        defaultValue: Constant(0));
  }

  final VerificationMeta _commissionValMeta =
      const VerificationMeta('commissionVal');
  GeneratedRealColumn _commissionVal;
  @override
  GeneratedRealColumn get commissionVal =>
      _commissionVal ??= _constructCommissionVal();
  GeneratedRealColumn _constructCommissionVal() {
    return GeneratedRealColumn('commission_val', $tableName, false,
        defaultValue: Constant(0));
  }

  final VerificationMeta _unitsSoldValMeta =
      const VerificationMeta('unitsSoldVal');
  GeneratedIntColumn _unitsSoldVal;
  @override
  GeneratedIntColumn get unitsSoldVal =>
      _unitsSoldVal ??= _constructUnitsSoldVal();
  GeneratedIntColumn _constructUnitsSoldVal() {
    return GeneratedIntColumn('units_sold_val', $tableName, false,
        defaultValue: Constant(0));
  }

  final VerificationMeta _yearMeta = const VerificationMeta('year');
  GeneratedIntColumn _year;
  @override
  GeneratedIntColumn get year => _year ??= _constructYear();
  GeneratedIntColumn _constructYear() {
    return GeneratedIntColumn('year', $tableName, false,
        defaultValue: Constant(0));
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, imagePath, rank, salesVal, commissionVal, unitsSoldVal, year];
  @override
  $SalesRanksTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'sales_ranks';
  @override
  final String actualTableName = 'sales_ranks';
  @override
  VerificationContext validateIntegrity(SalesRanksCompanion d,
      {bool isInserting = false}) {
    final context = VerificationContext();
    if (d.id.present) {
      context.handle(_idMeta, id.isAcceptableValue(d.id.value, _idMeta));
    } else if (id.isRequired && isInserting) {
      context.missing(_idMeta);
    }
    if (d.imagePath.present) {
      context.handle(_imagePathMeta,
          imagePath.isAcceptableValue(d.imagePath.value, _imagePathMeta));
    } else if (imagePath.isRequired && isInserting) {
      context.missing(_imagePathMeta);
    }
    if (d.rank.present) {
      context.handle(
          _rankMeta, rank.isAcceptableValue(d.rank.value, _rankMeta));
    } else if (rank.isRequired && isInserting) {
      context.missing(_rankMeta);
    }
    if (d.salesVal.present) {
      context.handle(_salesValMeta,
          salesVal.isAcceptableValue(d.salesVal.value, _salesValMeta));
    } else if (salesVal.isRequired && isInserting) {
      context.missing(_salesValMeta);
    }
    if (d.commissionVal.present) {
      context.handle(
          _commissionValMeta,
          commissionVal.isAcceptableValue(
              d.commissionVal.value, _commissionValMeta));
    } else if (commissionVal.isRequired && isInserting) {
      context.missing(_commissionValMeta);
    }
    if (d.unitsSoldVal.present) {
      context.handle(
          _unitsSoldValMeta,
          unitsSoldVal.isAcceptableValue(
              d.unitsSoldVal.value, _unitsSoldValMeta));
    } else if (unitsSoldVal.isRequired && isInserting) {
      context.missing(_unitsSoldValMeta);
    }
    if (d.year.present) {
      context.handle(
          _yearMeta, year.isAcceptableValue(d.year.value, _yearMeta));
    } else if (year.isRequired && isInserting) {
      context.missing(_yearMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SalesRank map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return SalesRank.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  Map<String, Variable> entityToSql(SalesRanksCompanion d) {
    final map = <String, Variable>{};
    if (d.id.present) {
      map['id'] = Variable<int, IntType>(d.id.value);
    }
    if (d.imagePath.present) {
      map['image_path'] = Variable<String, StringType>(d.imagePath.value);
    }
    if (d.rank.present) {
      map['rank'] = Variable<int, IntType>(d.rank.value);
    }
    if (d.salesVal.present) {
      map['sales_val'] = Variable<double, RealType>(d.salesVal.value);
    }
    if (d.commissionVal.present) {
      map['commission_val'] = Variable<double, RealType>(d.commissionVal.value);
    }
    if (d.unitsSoldVal.present) {
      map['units_sold_val'] = Variable<int, IntType>(d.unitsSoldVal.value);
    }
    if (d.year.present) {
      map['year'] = Variable<int, IntType>(d.year.value);
    }
    return map;
  }

  @override
  $SalesRanksTable createAlias(String alias) {
    return $SalesRanksTable(_db, alias);
  }
}

abstract class _$AldarDatabase extends GeneratedDatabase {
  _$AldarDatabase(QueryExecutor e)
      : super(const SqlTypeSystem.withDefaults(), e);
  $SalesRanksTable _salesRanks;
  $SalesRanksTable get salesRanks => _salesRanks ??= $SalesRanksTable(this);
  @override
  List<TableInfo> get allTables => [salesRanks];
}
