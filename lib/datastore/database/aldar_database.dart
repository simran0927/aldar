import 'package:moor_flutter/moor_flutter.dart';

import 'dashboard/sales_rank_table.dart';

part 'aldar_database.g.dart';

@UseMoor(tables: [SalesRanks])
// _$AppDatabase is the name of the generated class
class AldarDatabase extends _$AldarDatabase{
  AldarDatabase() : super(FlutterQueryExecutor.inDatabaseFolder(path: "db.sqlite",logStatements: true)){
  }

  // Bump this when changing tables and columns.
  // Migrations will be covered in the next part.
  @override
  int get schemaVersion => 1;
}

//class Tasks extends Table{
//// Every table column is a getter in the class.
//// autoIncrement automatically sets this to be the primary key.
//  IntColumn get id=>integer().autoIncrement()();
//  TextColumn get name =>text().withLength(min: 1,max: 50)();
//  DateTimeColumn get dueDate=> dateTime().nullable()();
//  BoolColumn get completed=> boolean().withDefault(Constant(false))();
//}


