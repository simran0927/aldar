import 'package:moor/moor.dart';

class SalesRanks extends Table{
  IntColumn get id=>integer().autoIncrement()();
  TextColumn get imagePath =>text().withLength(min: 1,max: 50)();
  IntColumn get rank=>integer().withDefault(Constant(0))();
  RealColumn get salesVal=>real().withDefault(Constant(0))();
  RealColumn get commissionVal=>real().withDefault(Constant(0))();
  IntColumn get unitsSoldVal=>integer().withDefault(Constant(0))();
  IntColumn get year=>integer().withDefault(Constant(0))();
}



