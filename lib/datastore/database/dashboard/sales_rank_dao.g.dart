// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sales_rank_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$SalesRankDaoMixin on DatabaseAccessor<AldarDatabase> {
  $SalesRanksTable get salesRanks => db.salesRanks;
}
