import 'package:aldar_broker/services/dashboard/sales_ranking_service.dart';
import 'package:moor_flutter/moor_flutter.dart';

import '../aldar_database.dart';
import 'sales_rank_table.dart';
part 'sales_rank_dao.g.dart';

@UseDao(tables: [SalesRanks])
class SalesRankDao extends DatabaseAccessor<AldarDatabase> with _$SalesRankDaoMixin {
  SalesRankDao(AldarDatabase db) : super(db);

  Future<List<SalesRank>> getSalesRanks() async {

    List<SalesRank> _salesRankData;
   _salesRankData= await getAllSalesRanks();
    if (_salesRankData.length > 0 ) {
      return _salesRankData;
    } else {
      await SalesRankingService().getSalesRanking().then((salesRankList) {
        salesRankList.forEach((_item) {
          _salesRankData = new List<SalesRank>();
          SalesRank salesRank = SalesRank(
              id: _item.id,
              imagePath: _item.imagePath,
              rank: _item.rank,
              salesVal: _item.salesVal,
              commissionVal: _item.commissionVal,
              unitsSoldVal: _item.unitsSoldVal,
              year: _item.year);
          insertSalesRank(salesRank);
          _salesRankData.add(salesRank);
        });
      });
      return _salesRankData;
    }
  }


  Future<List<SalesRank>> getAllSalesRanks() => select(salesRanks).get();
  Stream<List<SalesRank>> watchAllSalesRanks()=>select(salesRanks).watch();
  Future insertSalesRank(SalesRank salesrank) => into(salesRanks).insert(salesrank);
  Future updateSalesRank(SalesRank salesrank) => update(salesRanks).replace(salesrank);
  Future deleteSalesRank(SalesRank salesrank) => delete(salesRanks).delete(salesrank);
}
