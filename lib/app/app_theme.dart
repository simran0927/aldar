import 'package:flutter/material.dart';

class AppTheme {
  AppTheme._();

  //Colors used in the Aldar Application

  static const Color mainAppThemeColor = Color(0xFFFFFFFF);//white
  static const Color mainAppThemeColorShade = Color(0xFFe6e6e6);//background offwhite
  static const Color secondaryAppThemeColor = Color(0xFF000000);//black
  static const Color secondaryAppThemeColorLight = Color(0xFF686868);//grey

  static const Color pageBackground = Color(0xFFe6e6e6);
  static const Color appBarBackground = Color(0xFFFFFFFF);

  static const Color notWhite = Color(0xFFEDF0F2);
  static const Color nearlyWhite = Color(0xFFFEFEFE);
  static const Color white = Color(0xFFFFFFFF);
  static const Color nearlyBlack = Color(0xFF213333);
  static const Color grey = Color(0xFF3A5160);
  static const Color dark_grey = Color(0xFF313A44);
  static const Color yellow = Color(0xFFec9d1e);
  static const Color opaqueBlack = Color(0xFF686868);

  static const Color darkText = Color(0xFF253840);
  static const Color darkerText = Color(0xFF17262A);
  static const Color lightText = Color(0xFF4A6572);
  static const Color deactivatedText = Color(0xFF767676);
  static const Color dismissibleBackground = Color(0xFF364A54);
  static const Color chipBackground = Color(0xFFEEF1F3);
  //static const Color spacer = Color(0xFFF2F2F2);

  static const Color tabIndicatorColor = secondaryAppThemeColor;
  static const Color tabunselectedLabelColor = nearlyBlack;


  //Text Styles used in the Aldar Application

  static const String fontName = 'WorkSans';

  static const TextTheme textTheme = TextTheme(
    display1: display1,
    headline: headline,
    title: title,
    subtitle: subtitle,
    body2: body2,
    body1: body1,
    caption: caption,
  );

  static const TextStyle display1 = TextStyle( // h4 -> display1
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 36,
    letterSpacing: 0.4,
    height: 0.9,
    color: darkerText,
  );

  static const TextStyle headline = TextStyle( // h5 -> headline
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 24,
    letterSpacing: 0.27,
    color: darkerText,
  );

  static const TextStyle title = TextStyle( // h6 -> title
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 18,
    letterSpacing: 0.18,
    color: secondaryAppThemeColor,
  );

  static const TextStyle subtitle = TextStyle( // subtitle2 -> subtitle
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.04,
    color: darkText,
  );

  static const TextStyle body2 = TextStyle( // body1 -> body2
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: 0.2,
    color: secondaryAppThemeColor,
  );

  static const TextStyle body1 = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle caption = TextStyle( // Caption -> caption
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    letterSpacing: 0.2,
    color: lightText, // was lightText
  );

  static const TextStyle tabtitle = text_mediumSemiBold;


  static const TextStyle text_largeNormal = TextStyle( // body2 -> body1
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 20,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_largeSemiBold = TextStyle( // body1 -> body2
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 20,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_largeBold = TextStyle( // body1 -> body2
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 20,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_mediumNormal = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 18,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_mediumSemiBold = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 18,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_mediumBold = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 18,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_normal = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 16,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_normalSemiBold = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );
  static const TextStyle text_normalBold = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.bold,
    fontSize: 16,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_smallNormal = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.normal,
    fontSize: 14,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_smallSemiBold = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );

  static const TextStyle text_smallBold = TextStyle(
    fontFamily: fontName,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    letterSpacing: -0.05,
    color: secondaryAppThemeColor,
  );


  //Padding, Margins, Divider and EmptySpace used in the Aldar Application

  static const EdgeInsets main_block_padding = EdgeInsets.only(left: 10, right: 10, top: 10);
  static const EdgeInsets card_margin = EdgeInsets.only(top: 5,bottom: 5);
  static const EdgeInsets alert_dialog_titlebar_padding = EdgeInsets.only(top: 5,bottom: 5);

  static const Divider primaryColorDivider = Divider(
    color: mainAppThemeColorShade,
    thickness: 1,
  );

  static const Divider secondaryColorDivider = Divider(
    color: nearlyBlack,
    thickness: 1,
  );

  static const SizedBox emptyBoxSpace = SizedBox(height: 20);

}
