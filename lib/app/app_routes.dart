import '../screens/properties/properties.dart';
import '../screens/dashboard/dashboard.dart';
import '../screens/support/help_screen.dart';
import '../screens/support/feedback_screen.dart';
import '../screens/settings/settings_screen.dart';
import '../screens/social/invite_friends.dart';
import '../screens/support/about_us.dart';
import '../screens/social/rate_app.dart';
import '../screens/properties/property_details.dart';
import '../screens/splash/splash_screen.dart';
import '../screens/auth/login_screen.dart';

import 'package:flutter/material.dart';

class Router {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case LoginScreen.routeName:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case DashboardScreen.routeName:
        return MaterialPageRoute(builder: (_) => DashboardScreen());
      case PropertiesScreen.routeName:
        return MaterialPageRoute(builder: (_) => PropertiesScreen());
      case PropertyDetails.routeName:
        return MaterialPageRoute(builder: (_) => PropertyDetails());
      case InviteFriends.routeName:
        return MaterialPageRoute(builder: (_) => InviteFriends());
      case SettingsScreen.routeName:
        return MaterialPageRoute(builder: (_) => SettingsScreen());
      case RateApp.routeName:
        return MaterialPageRoute(builder: (_) => RateApp());
      case FeedbackScreen.routeName:
        return MaterialPageRoute(builder: (_) => FeedbackScreen());
      case HelpScreen.routeName:
        return MaterialPageRoute(builder: (_) => HelpScreen());
      case AboutUS.routeName:
        return MaterialPageRoute(builder: (_) => AboutUS());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                  child: Text('No route defined for ${settings.name}')),
            ));
    }
  }
  static Map<String, WidgetBuilder> routes(){
    return {
      PropertiesScreen.routeName:(ctx) => PropertiesScreen(),
      DashboardScreen.routeName:(ctx) => DashboardScreen(),
      HelpScreen.routeName: (ctx) => HelpScreen(),
      FeedbackScreen.routeName: (ctx) => FeedbackScreen(),
      SettingsScreen.routeName: (ctx) => SettingsScreen(),
      InviteFriends.routeName: (ctx) => InviteFriends(),
      AboutUS.routeName: (ctx) => AboutUS(),
      RateApp.routeName: (ctx) => RateApp(),
      PropertyDetails.routeName: (ctx) => PropertyDetails(),
      LoginScreen.routeName:(ctx) => LoginScreen()
    };
  }

}