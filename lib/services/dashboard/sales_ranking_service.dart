import 'dart:convert';
import 'package:aldar_broker/services/ApiHitter.dart';

import '../../commons/constants/app_constants.dart';
import '../../model/dashboard/sales_ranking_item.dart';
import '../rest_client.dart';


class SalesRankingService {
  //final String salesRankingAPI = AppConstants.GET_SALES_RANKING_API;
// rest-client instance
  final RestClient _restClient = new RestClient();

  // injecting restclient instance
  //SalesRankingService(this._restClient);

  /*Future<List<SalesRankItem>> getSalesRanking() async {
    try {
      final res = await _restClient.get(AppConstants.GET_SALES_RANKING_API);
      return List<SalesRankItem>.from(res.map((i) => SalesRankItem.fromJson(i)));//res.map((i) => SalesRankItem.fromJson(i)).toList();
    } catch (e) {
      print(e.toString());
      throw e;
    }
  }*/
   Future<List<SalesRankItem>> getSalesRanking() async {
     ApiResponse apiResponse = await ApiHitter().getApiResponse(
         AppConstants.GET_SALES_RANKING_API);
     try {
       return apiResponse.status
           ? List<SalesRankItem>.from(
           apiResponse.response.data.map((i) => SalesRankItem.fromJson(i)))
           : null;
     }catch(e){
       print(e.toString());
       return null;
     }
   }
}