import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'app/app_routes.dart';
import 'app/app_theme.dart';
import 'datastore/database/aldar_database.dart';
import 'datastore/database/dashboard/sales_rank_dao.dart';
import 'provider/dashboard/sales_ranking_provider.dart';
import 'screens/properties/properties.dart';
import 'screens/dashboard/dashboard.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'app/app_translations_delegate.dart';
import 'app/application.dart';
import 'screens/support/help_screen.dart';
import 'screens/support/feedback_screen.dart';
import 'screens/settings/settings_screen.dart';
import 'package:provider/provider.dart';
import './provider/drawer_state.dart';
import 'screens/social/invite_friends.dart';
import 'screens/support/about_us.dart';
import 'screens/social/rate_app.dart';
import 'screens/properties/property_details.dart';
import './provider/property_provider.dart';
import 'provider/auth/auth_provider.dart';
import 'screens/splash/splash_screen.dart';
import 'screens/auth/login_screen.dart';


void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]).then((_) => runApp(MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  AppTranslationsDelegate _newLocaleDelegate;

  @override
  void initState() {
    super.initState();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness:
          Platform.isAndroid ? Brightness.dark : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: DrawerState()),
        ChangeNotifierProvider.value(value: PropertProvider()),
        ChangeNotifierProvider.value(value: AuthProvider()),
        ChangeNotifierProvider.value(value: SalesRankingProvider()),
        Provider(builder: (_) => SalesRankDao(AldarDatabase()))
      ],
      child: MaterialApp(
        title: 'Flutter UI',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          textTheme: AppTheme.textTheme,
          platform: TargetPlatform.iOS,
        ),
        home: SplashScreen(),
        routes: Router.routes(),
        localizationsDelegates: [
          _newLocaleDelegate,
          const AppTranslationsDelegate(),
          //provides localised strings
          GlobalMaterialLocalizations.delegate,
          //provides RTL support
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: application.supportedLocales(),
      ),
    );
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}

class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}
