import '../../model/dashboard/sales_ranking_item.dart';

import '../../services/dashboard/sales_ranking_service.dart';
import 'package:flutter/material.dart';

class SalesRankingProvider with ChangeNotifier {

  List<SalesRankItem> _salesRankData =new List<SalesRankItem>();

  List<SalesRankItem> get items {
    // if (_showFavoritesOnly) {
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }
    return [..._salesRankData];
  }

  Future<void> fetchAndSetSalesRanks() async{
    await SalesRankingService().getSalesRanking().then((result) {
      _salesRankData =result as List<SalesRankItem>;
      notifyListeners();
    });
  }

}
