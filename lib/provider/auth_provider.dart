import 'package:flutter/material.dart';
class AuthProvider with ChangeNotifier{
  bool isLoggedIn = false;

  bool getLoggedInStatus(){
    return isLoggedIn;
  }

  void updateLoggedInsStatus(status){
    this.isLoggedIn = status;
    notifyListeners();
  }

  
}