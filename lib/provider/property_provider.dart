import 'package:flutter/material.dart';
import '../model/property.dart';

class PropertProvider with ChangeNotifier {
  List<Property> _items = [
    Property(
        id: 1,
        imagePath: 'assets/hotel/hotel_1.png',
        titleTxt: 'Grand Royal Hotel',
        subTxt: 'Wembley, London',
        detailTxt:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        bed: 3,
        price: 3000,
        squareFeet: 1500),
    Property(
        id: 2,
        imagePath: 'assets/hotel/hotel_2.png',
        titleTxt: 'Queen Hotel',
        subTxt: 'Wembley, London',
        detailTxt:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        bed: 3,
        price: 4500,
        squareFeet: 2000),
    Property(
        id: 3,
        imagePath: 'assets/hotel/hotel_3.png',
        titleTxt: 'Grand Royal Hotel',
        subTxt: 'Wembley, London',
        detailTxt:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        bed: 3,
        price: 2200,
        squareFeet: 1000),
    Property(
        id: 4,
        imagePath: 'assets/hotel/hotel_4.png',
        titleTxt: 'Queen Hotel',
        subTxt: 'Wembley, London',
        detailTxt:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        bed: 3,
        price: 3000,
        squareFeet: 2100),
    Property(
        id: 5,
        imagePath: 'assets/hotel/hotel_5.png',
        titleTxt: 'Grand Royal Hotel',
        subTxt: 'Wembley, London',
        detailTxt:
            'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        bed: 3,
        price: 3000,
        squareFeet: 1500)
  ];

  List<Property> get items {
    return [..._items];
  }
}
