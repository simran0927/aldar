import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';

class DrawerState with ChangeNotifier {
  
  DrawerIndex _drawwerIndex = DrawerIndex.Dashboard;

  DrawerState();
  getCurrentDrawerIndex() => _drawwerIndex;

  void setDrawerIndex(DrawerIndex index){
    _drawwerIndex = index;
    notifyListeners();
  }

}